﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlags.Models
{
    public class Pais
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Capital { get; set; }
        public decimal Populacao { get; set; }
        public decimal Area { get; set; }
        public int Peso { get; set; }
        public int? RndCol { get; set; }
    }
}