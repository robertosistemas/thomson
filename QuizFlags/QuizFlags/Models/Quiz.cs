﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlags.Models
{
    public class Quiz
    {
        public int Indice { get; set; }
        public Pais PaisBandeira { get; set; }
        public Pais PaisEscolhido { get; set; }
        public List<Pais> Paises { get; set; }
        public int? IdResposta { get; set; }
        public int PontosGanhos { get; set; }
        public int PontosPerdidos { get; set; }

        public void CalculaPontos()
        {
            if (IdResposta.HasValue)
            {
                PaisEscolhido = Paises[IdResposta.Value];
                if (PaisEscolhido.Id == PaisBandeira.Id)
                {
                    PontosGanhos = (PaisBandeira.Peso * 10);
                }
                else
                {
                    PontosPerdidos = (PaisBandeira.Peso * 10);
                }
            }
        }
    }
}