﻿using QuizFlags.Config;
using QuizFlags.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlags.Repositorios
{
    public class PaisRepository : IPaisRepository
    {
        public List<Pais> ObterTodosPaises()
        {
            List<Pais> lst = new List<Pais>();
            using (System.IO.StreamReader arquivo = new System.IO.StreamReader(AppConfig.ArquivoPaises))
            {
                string linha;
                char[] separador = { Convert.ToChar(",") };
                while ((linha = arquivo.ReadLine()) != null)
                {
                    string[] strAr = linha.Split(separador);
                    Pais pais = new Pais()
                    {
                        Id = Convert.ToInt32(strAr[0]),
                        Nome = strAr[1],
                        Capital = strAr[2],
                        Populacao = Convert.ToDecimal(strAr[3]),
                        Area = Convert.ToDecimal(strAr[4]),
                        Peso = Convert.ToInt32(strAr[5])
                    };
                    lst.Add(pais);
                }
            }
            return lst;
        }

        public Pais Clonar(Pais pais)
        {
            return new Pais()
            {
                Id = pais.Id,
                Nome = string.Copy(pais.Nome),
                Capital = string.Copy(pais.Capital),
                Populacao = pais.Populacao,
                Area = pais.Area,
                Peso = pais.Peso,
                RndCol = pais.RndCol.Value
            };
        }

        private void permuta(Pais de, Pais para)
        {
            Pais paisDe = Clonar(de);
            Pais paisPara = Clonar(para);

            de = paisPara;
            para = paisDe;

        }

        public List<Pais> RandomizeLista(List<Pais> paises)
        {
            int n;
            int count = paises.Count - 1;
            Random _rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i <= count; i++)
            {
                n = _rand.Next(count);
                paises[i].RndCol = n;
            }

            count = paises.Count - 1;

            for (int i = 0; i <= count; i++)
            {
                n = _rand.Next(count);
                permuta(paises[i], paises[n]);
            }

            paises = paises.OrderBy(item => item.RndCol).ToList();

            return paises;
        }
    }
}