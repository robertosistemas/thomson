﻿using QuizFlags.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizFlags.Repositorios
{
    public interface IPaisRepository
    {
        List<Pais> ObterTodosPaises();
        List<Pais> RandomizeLista(List<Pais> paises);
        Pais Clonar(Pais pais);
    }
}
