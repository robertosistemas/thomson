﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using QuizFlags.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizFlags.Installers
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IPaisRepository>()
                    .ImplementedBy<PaisRepository>()
                    .LifestyleTransient());
        }
    }
}