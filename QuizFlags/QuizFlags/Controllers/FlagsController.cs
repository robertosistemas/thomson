﻿using QuizFlags.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizFlags.Controllers
{
    public class FlagsController : Controller
    {
        // GET: Flags
        [Route("Flags")]
        [Route("Flags/{id}")]
        [Route("Flags/Index")]
        [Route("Flags/Index/{id}")]
        public ActionResult Index(int? id)
        {
            return View();
        }

        [Route("Flags/result")]
        public ActionResult result()
        {
            List<Quiz> quizResult = new List<Quiz>();

            // Tenta obter a lista de quiz já mostrado da sessão do usuário
            Dictionary<int, Quiz> quizzes = HttpContext.Session["quizzes"] as Dictionary<int, Quiz>;

            if (quizzes == null)
            {
                return new RedirectResult("~/flags");
            }

            foreach (var item in quizzes)
            {
                var quiz = item.Value;

                quiz.CalculaPontos();

                quizResult.Add(quiz);
            }
            return View(quizResult);
        }

    }
}
