﻿using QuizFlags.Models;
using QuizFlags.Repositorios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace QuizFlags.Controllers
{
    public class QuizController : ApiController
    {

        private IPaisRepository _paisRepository;

        public QuizController(IPaisRepository paisRepository)
        {
            _paisRepository = paisRepository;
        }

        // POST: api/EnviarResultado
        [Route("api/IniciarQuiz")]
        [ResponseType(typeof(void))]
        public void IniciarQuiz()
        {
            if (HttpContext.Current.Cache["paisesTodos"] != null)
            {
                HttpContext.Current.Cache.Remove("paisesTodos");
            }
            if (HttpContext.Current.Session["paisesMostrados"] != null)
            {
                HttpContext.Current.Session.Remove("paisesMostrados");
            }
            if (HttpContext.Current.Session["quizzes"] != null)
            {
                HttpContext.Current.Session.Remove("quizzes");
            }
        }

        // GET: api/Quiz/5
        [Route("api/Quiz/{indice}")]
        [ResponseType(typeof(Quiz))]
        public IHttpActionResult Get(int? indice = 1)
        {

            // Verifica se o índice está dentro da faixa permitida
            if (indice < 1 || indice > 10)
            {
                return BadRequest("Índice deve estar entre 1 e 10.");
            }

            // Tenta obter a lista de quiz já mostrado da sessão do usuário
            Dictionary<int, Quiz> quizzes = HttpContext.Current.Session["quizzes"] as Dictionary<int, Quiz>;
            if (quizzes == null || ((indice <= 1 && quizzes.Count > 1) || quizzes.ContainsKey(indice.Value) && quizzes[indice.Value].IdResposta == null))
            {
                quizzes = new Dictionary<int, Quiz>();
                if (HttpContext.Current.Session["quizzes"] != null)
                {
                    HttpContext.Current.Session.Remove("quizzes");
                }
                if (HttpContext.Current.Session["paisesMostrados"] != null)
                {
                    HttpContext.Current.Session.Remove("paisesMostrados");
                }
            }

            // Tenta obter a lista de países já mostrado da sessão do usuário
            Dictionary<int, Pais> _paisesMostrados = HttpContext.Current.Session["paisesMostrados"] as Dictionary<int, Pais>;
            if (_paisesMostrados == null)
            {
                _paisesMostrados = new Dictionary<int, Pais>();
                if (HttpContext.Current.Session["paisesMostrados"] != null)
                {
                    HttpContext.Current.Session.Remove("paisesMostrados");
                }
            }

            // Tenta obter a lista de paises do cache
            List<Pais> _paisesTodos = HttpContext.Current.Cache["paisesTodos"] as List<Pais>;
            if (_paisesTodos == null)
            {
                // Se não encontrou no cache, então carrega do arquivo csv
                _paisesTodos = _paisRepository.ObterTodosPaises();
            }
            // Classifica a lista de países randomicamente
            _paisesTodos = _paisRepository.RandomizeLista(_paisesTodos);
            HttpContext.Current.Cache["paisesTodos"] = _paisesTodos;

            Quiz quiz = null;

            // Verifica se o índice informado já está no dicionário de quiz repondidos
            if (quizzes.ContainsKey(indice.Value))
            {
                quiz = quizzes[indice.Value];
            }
            else
            {
                if (_paisesMostrados.Count < 10)
                {
                    Dictionary<int, Pais> paisesMostrarDic = new Dictionary<int, Pais>();

                    //Procura pelo primeiro pais que ainda não foi mostrado
                    int i = 0;
                    int paisKey = 0;
                    while (paisesMostrarDic.Count < 1)
                    {
                        paisKey = _paisesTodos[i].Id;
                        if (!_paisesMostrados.ContainsKey(paisKey))
                        {
                            paisesMostrarDic.Add(paisKey, _paisRepository.Clonar(_paisesTodos[i]));
                        }
                        i++;
                    }

                    // Procura por mais 3 países quaisquer diferente do primeiro selecionado e adiciona na lista
                    i = 0;
                    while (paisesMostrarDic.Count < 4)
                    {
                        if (!paisesMostrarDic.ContainsKey(_paisesTodos[i].Id))
                        {
                            paisesMostrarDic.Add(_paisesTodos[i].Id, _paisRepository.Clonar(_paisesTodos[i]));
                        }
                        i++;
                    }

                    // Define o modelo de Quiz com o pais para mostrar a bandeira, e a lista classificada
                    // randomicamente de 4 países para a escolha do usuário

                    List<Pais> paisesLst = new List<Pais>();
                    foreach (var item in paisesMostrarDic)
                    {
                        paisesLst.Add(item.Value);
                    }
                    quiz = new Quiz()
                    {
                        Indice = indice.Value,
                        PaisBandeira = _paisRepository.Clonar(paisesMostrarDic[paisKey]),
                        Paises = _paisRepository.RandomizeLista(paisesLst)
                    };

                }

            }

            return Ok(quiz);
        }

        // POST: api/Quiz
        /// <summary>
        /// public async Task<IHttpActionResult> Post(Quiz quiz)
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        [Route("api/Quiz")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Post(Quiz quiz)
        {
            // Verifica se o índice informado já está no dicionário de quiz repondidos
            if (quiz.IdResposta.HasValue) // && quizzes != null && quizzes.ContainsKey(quiz.Indice))
            {
                quiz.PaisEscolhido = _paisRepository.Clonar(quiz.Paises[quiz.IdResposta.Value]);

                // Tenta obter a lista de países já mostrado da sessão do usuário
                Dictionary<int, Pais> _paisesMostrados = HttpContext.Current.Session["paisesMostrados"] as Dictionary<int, Pais>;
                if (_paisesMostrados == null)
                {
                    _paisesMostrados = new Dictionary<int, Pais>();
                }
                if (!_paisesMostrados.ContainsKey(quiz.PaisBandeira.Id))
                {
                    // Incluí na lista de países já mostrados para não mostrar esta bandeira novamente
                    _paisesMostrados.Add(quiz.PaisBandeira.Id, _paisRepository.Clonar(quiz.PaisBandeira));
                }
                HttpContext.Current.Session["paisesMostrados"] = _paisesMostrados;

                // Tenta obter a lista de quiz já mostrado da sessão do usuário
                Dictionary<int, Quiz> quizzes = HttpContext.Current.Session["quizzes"] as Dictionary<int, Quiz>;
                if (quizzes == null)
                {
                    quizzes = new Dictionary<int, Quiz>();
                }
                // adiciona quiz no dicionário de quiz já respondido
                quizzes[quiz.Indice] = quiz;

                HttpContext.Current.Session["quizzes"] = quizzes;

            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EnviarResultado
        [Route("api/EnviarResultado")]
        [ResponseType(typeof(void))]
        public IHttpActionResult EnviarResultado([FromBody]string email)
        {
            // Tenta obter a lista de quiz já mostrado da sessão do usuário
            Dictionary<int, Quiz> quizzes = HttpContext.Current.Session["quizzes"] as Dictionary<int, Quiz>;
            if (quizzes != null)
            {
                try
                {
                    string smtpHost = ConfigurationManager.AppSettings["AppSmtpHost"];
                    int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["AppSmtpPort"]);
                    string mailFrom = ConfigurationManager.AppSettings["AppMailFrom"];

                    if (string.IsNullOrWhiteSpace(smtpHost) || smtpHost.Equals("smtp.exemplo.com")
                        || string.IsNullOrWhiteSpace(mailFrom) || mailFrom.Equals("email@exemplo.com"))
                    {
                        return BadRequest("Para o envio de e-mail, é necessário informar as configurações de (AppSmtpHost, AppSmtpPort, AppMailFrom) no web.config.");
                    }

                    string mailTo = email;
                    string mailSubject = "Resultado do Quiz";
                    StringBuilder mailBody = new StringBuilder();

                    foreach (var item in quizzes)
                    {
                        string msg = string.Empty;

                        var quiz = item.Value;

                        quiz.CalculaPontos();

                        if (quiz.IdResposta.HasValue && quiz.PaisEscolhido != null)
                        {
                            if (quiz.PontosGanhos == 0)
                            {
                                msg = string.Format("Bandeira {0}: Você escolheu {1}, e o correto seria: {2}. :( - (deixou de ganhar {2} pontos)", quiz.Indice, quiz.PaisEscolhido.Nome, quiz.PaisBandeira.Nome, quiz.PontosGanhos);
                            }
                            else
                            {
                                msg = string.Format("Bandeira {0}: Você escolheu {1}, e estava correto. :) - (ganhou {2} pontos)", quiz.Indice, quiz.PaisEscolhido.Nome, quiz.PontosGanhos);
                            }
                        }

                        mailBody.AppendFormat("<tr><td><span>{0}</span></td></tr>", msg);
                    }


                    using (SmtpClient smtpClient = new SmtpClient(smtpHost, smtpPort))
                    {
                        MailMessage mailMessage = new MailMessage(mailFrom, mailTo, mailSubject, mailBody.ToString());
                        mailMessage.IsBodyHtml = true;
                        smtpClient.Send(mailMessage);
                    }

                    return StatusCode(HttpStatusCode.NoContent);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return NotFound();
        }

    }
}
