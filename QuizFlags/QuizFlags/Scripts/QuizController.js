﻿(function () {
    'use strict';

    // App Angular
    var quizApp = angular.module('quizApp', ['ngMaterial']);

    // Configurações globais
    var baseUrlDef = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

    quizApp.value('config', {
        baseUrl: baseUrlDef,
        timeout: -1
    });

    // Interceptando mensagens
    quizApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q) {
            return {
                'response': function (response) {
                    //Will only be called for HTTP up to 300
                    //console.log(response);
                    return response;
                },
                'responseError': function (rejection) {
                    //console.log(rejection);
                    if (rejection.status === 401) {
                        //location.reload();
                    }
                    return $q.reject(rejection);
                }
            };
        });
    }]);

    // Service
    quizApp.factory('quizService', quizService);

    quizService.$inject = ['$http', 'config'];

    function quizService($http, config) {

        var postIniciarQuiz = function () {
            return $http.post(config.baseUrl + '/api/IniciarQuiz', null, { timeout: config.timeout });
        };

        var getQuiz = function (indice) {
            return $http.get(config.baseUrl + '/api/Quiz/' + indice, null, { timeout: config.timeout });
        };

        var postQuiz = function (modelQuiz) {
            return $http.post(config.baseUrl + '/api/Quiz', modelQuiz, { timeout: config.timeout });
        };

        return {
            postIniciarQuiz: postIniciarQuiz,
            getQuiz: getQuiz,
            postQuiz: postQuiz
        };

    }

    // Controller
    quizApp.controller('quizController', quizController);

    quizController.$inject = ['$scope', '$location', '$window', '$mdDialog', 'config', 'quizService'];

    function quizController($scope, $location, $window, $mdDialog, config, quizService) {

        var vm = this;

        vm.Quiz = {};

        $scope.loading = true;

        function showAlert(titulo, conteudo, textoBotao) {
            alert = $mdDialog.alert()
                .title(titulo)
                .textContent(conteudo)
                .ok(textoBotao);
            $mdDialog
                .show(alert)
                .finally(function () {
                    alert = undefined;
                });
        }

        vm.postIniciarQuiz = function () {
            $scope.loading = true;
            quizService.postIniciarQuiz()
                .then(function (resultado) {
                    $window.location = config.baseUrl + '/flags';
                }, function (error) {
                    console.log(error);
                    showAlert('Erro', 'Erro ao tentar inicializar o Quiz. ' + error.data.Message, 'Fechar');
                }).finally(function () {
                    $scope.loading = false;
                });
        };

        vm.getQuiz = function (indice) {
            $scope.loading = true;
            quizService.getQuiz(indice)
                .then(function (resultado) {
                    delete vm.quiz;
                    $scope.quizForm.$setPristine();
                    vm.Quiz = resultado.data;
                }, function (error) {
                    console.log(error);
                    showAlert('Erro', 'Erro ao tentar obter dados. ' + error.data.Message, 'Fechar');
                }).finally(function () {
                    $scope.loading = false;
                });
        };

        vm.postQuiz = function (quiz) {
            $scope.loading = true;
            quizService.postQuiz(quiz)
                .then(function (data) {
                    showAlert('Erro', 'Quiz atualizado com sucesso.', 'Fechar');
                }, function (error) {
                    console.log(error);
                    showAlert('Erro', 'Erro ao tentar atualizar Quiz. ' + error.data.Message, 'Fechar');
                }).finally(function () {
                    $scope.loading = false;
                });
        };

        vm.proximoQuiz = function (quiz) {
            var indice = parseInt(quiz.Indice);
            if (indice >= 1 && indice <= 10) {
                $scope.loading = true;
                quizService.postQuiz(quiz)
                    .then(function (data) {
                        if (indice >= 1 && indice <= 9) {
                            vm.getQuiz(indice + 1);
                        }
                        if (indice === 10) {
                            //$location.path('/flags/result');
                            //$location.replace();
                            $window.location = config.baseUrl + '/flags/result';
                        }
                    }, function (error) {
                        console.log(error);
                        showAlert('Erro', 'Erro ao tentar buscar o próximo Quiz. ' + error.data.Message, 'Fechar');
                    }).finally(function () {
                        $scope.loading = false;
                    });
            }
        };

        vm.selecionaPais = function (pais) {
            var idResposta = vm.Quiz.Paises.indexOf(pais);
            if (idResposta > -1) {
                vm.Quiz.IdResposta = idResposta;
            }
        };

        // Busca o primeiro Quiz
        function activate() {
            vm.getQuiz(1);
        }

        activate();

    }

})();
