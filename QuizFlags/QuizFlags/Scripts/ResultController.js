﻿(function () {
    'use strict';

    // App Angular
    var quizApp = angular.module('quizApp', ['ngMaterial']);

    // Configurações globais
    var baseUrlDef = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

    quizApp.value('config', {
        baseUrl: baseUrlDef,
        timeout: -1
    });

    // Service
    quizApp.factory('quizService', quizService);

    quizService.$inject = ['$http', 'config'];

    function quizService($http, config) {

        var postIniciarQuiz = function () {
            return $http.post(config.baseUrl + '/api/IniciarQuiz', null, { timeout: config.timeout });
        };

        var postEnviarResultado = function (email) {
            return $http.post(config.baseUrl + '/api/EnviarResultado', JSON.stringify(email), { timeout: config.timeout });
        };

        return {
            postIniciarQuiz: postIniciarQuiz,
            postEnviarResultado: postEnviarResultado
        };

    }

    // Controller
    quizApp.controller('resultController', resultController);

    resultController.$inject = ['$scope', '$location', '$window', '$mdDialog', 'config', 'quizService'];

    function resultController($scope, $location, $window, $mdDialog, config, quizService) {

        var vm = this;

        vm.Quiz = {};

        $scope.loading = true;

        function showAlert(titulo, conteudo, textoBotao) {
            alert = $mdDialog.alert()
                .title(titulo)
                .textContent(conteudo)
                .ok(textoBotao);
            $mdDialog
                .show(alert)
                .finally(function () {
                    alert = undefined;
                });
        }

        vm.iniciarQuiz = function () {
            $scope.loading = true;
            quizService.postIniciarQuiz()
                .then(function (resultado) {
                    $window.location = config.baseUrl + '/flags';
                }, function (error) {
                    console.log(error);
                    showAlert('Erro', 'Erro ao tentar inicializar o Quiz. ' + error.data.Message, 'Fechar');
                }).finally(function () {
                    $scope.loading = false;
                });
        };

        vm.enviarResultado = function (ev) {
            $scope.loading = true;

            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.prompt()
                .title('Quiz - Flags')
                .textContent('Digite um e-mail para compartilhar o resultado.')
                .placeholder('e-mail')
                .ariaLabel('E-mail:')
                .initialValue('')
                .targetEvent(ev)
                .required(true)
                .ok('Envia')
                .cancel('Cancela');

            $mdDialog.show(confirm).then(function (email) {
                $scope.status = 'email:' + email + '.';
                if (email !== null && email !== '') {
                    quizService.postEnviarResultado(email)
                        .then(function (resultado) {
                            showAlert('Mensagem', "Resultado enviado para o email: " + email, 'Fechar');
                        }, function (error) {
                            console.log(error);
                            showAlert('Alerta', error.data.Message, 'Fechar');
                        }).finally(function () {
                            $scope.loading = false;
                        });
                }
            }, function () {
                $scope.status = 'Cancelou';
            });
        };

    }

})();
