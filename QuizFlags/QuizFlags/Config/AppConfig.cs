﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace QuizFlags.Config
{
    public static class AppConfig
    {
        public static string ArquivoPaises => ArquivoPaisesPath();

        private static string ArquivoPaisesPath()
        {
            return HostingEnvironment.MapPath("~/App_Data/paises.csv");
        }
    }
}