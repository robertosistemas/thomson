﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace QuizFlags.Helpers.Html
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText, string height)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttribute("height", height);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString NgImage(this HtmlHelper helper, string src, string altText, string height)
        {
            var builder = new TagBuilder("img");
            //builder.MergeAttribute("ng-src", src);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttribute("height", height);

            builder.Attributes.Add("ng-src", src);
            //builder.Attributes.Add("ng-src", VirtualPathUtility.ToAbsolute(src));

            //return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing).Replace("src", "ng-src"));
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

        static string MakeImageSrcData(string filename)
        {
            string arq = HostingEnvironment.MapPath(string.Format("~{0}", filename));
            using (FileStream fs = new FileStream(arq, FileMode.Open, FileAccess.Read))
            {
                byte[] filebytes = new byte[fs.Length];
                fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                return "data:image/png;base64," + Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
            }
        }

    }
}